package DTUClientService;

import java.util.Scanner;

public class SimpleCalculator {
    public static void main(String[] args) {
        int number1;
        int number2;
        Scanner input = new Scanner(System.in);
        System.out.println("Give the first number: ");
        number1 = input.nextInt();
        System.out.println("Give the second number: ");
        number2 = input.nextInt();
        System.out.println(number1 + " + " + number2 + " = " + addNumbers(number1, number2));

    }

    public static int addNumbers(int n1, int n2){
        int result;
        result = n1 + n2;
        return result;
    }
}
