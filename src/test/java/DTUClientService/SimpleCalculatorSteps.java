package DTUClientService;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;

public class SimpleCalculatorSteps {
    int int1,int2;
    Integer total = 0;
    SimpleCalculator calc = new SimpleCalculator();

    @Given("that user provides {int}")
    public void that_user_provides(Integer int1) {
        this.int1 = int1;
    }

    @Given("and user provides {int}")
    public void and_user_provides(Integer int2) {
        this.int2 = int2;
    }

    @When("and the addNumbers method is called")
    public void and_the_add_numbers_method_is_called() {
        total = calc.addNumbers(this.int1, this.int2);
    }

    @Then("the result should be {int}")
    public void the_result_should_be(Integer result) {
        assertEquals(result, this.total);
    }

}
